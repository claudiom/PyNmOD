#! /usr/bin/env python

import os, io
homepath = os.getenv('HOME')
os.system('clear')

print 'Welcome to PyNmOD (Python Nmap OS Detector)! \n'
prompt = raw_input('Please enter the TCP/IP address or IP range for Nmap. \n("man nmap" for proper TCP/IP formats): ')
	
print 'Scanning.  Please wait... \n'
command = os.system('nmap -F -O ' + prompt + ' > ' + homepath + '/nmapscanresults.txt')

fh = open(homepath + '/nmapscanresults.txt')
#fh = (homepath + '/nmapscanresults.txt')

os = list()
host = list()
pc_info = dict()
scanlist = list()

for line in fh:
	if line.startswith('Nmap scan report for'):
		line = line.rstrip()
		line = line.split()
		host.append(line[4])
	elif line.startswith('Running: '):
		line = line.rstrip()
		line = line[len('Running: '):]
		os.append(line)
	elif line.startswith('Too many fingerprints'):
		line = line.rstrip()
		os.append(line)
	elif line.startswith('No exact OS matches for host'):
		line = line.rstrip()
		os.append(line[:len('No exact OS matches for host')])
		

num = 0
if str(len(os)) == 1:
	print 'PyNmOD has found ' + str(len(os)) + ' item. \n'
else:
	print 'PyNmOD has found ' + str(len(os)) + ' items. \n'

for computers in host:
	if num <= len(os) - 1:
		pc_info[computers] = pc_info.get(computers, os[num])
		num += 1
	else:
		break
		
for pc, os in pc_info.items():
	host_os = (pc, os)
	scanlist.append(host_os)
	scanlist.sort()
for pc, os in scanlist:
	print pc, ':', os

while True:
	saveprompt = raw_input('Would you like to save this to a file? (Y/N): ')
	if saveprompt is 'Y' or saveprompt is 'y':
		savefile = open(homepath + '/pynmod-results.csv', 'w')
		if str(len(os)) == 1:
        		savefile.write('PyNmOD has found ' + str(len(os)) + ' item. \n')
		else:
        		savefile.write('PyNmOD has found ' + str(len(os)) + ' items. \n')
		for pc, os in scanlist:
			savefile.write(pc + ',' + os + '\n')
		print 'Data saved in ' + homepath + '/pynmod-results.csv'
		savefile.close()
		quit()
	elif saveprompt is 'N' or saveprompt is 'n':
		quit()
	else:
		print 'Please select Y or N.\n'
		continue


