PyNmOD: The Python Nmap OS Detector
===================================

This requires Nmap.  Please make sure to have Nmap installed before running.  The "sudo" command is only needed for the PyNmOD.py script.  Requires root privileges as per Nmap.

Files:
PyNmOD.py - uses "sudo"
PyNmOD-nosudo.py - does not require "sudo" but must be run as root
